<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>searchResult</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />

<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>


<body>
	<!-- ヘッダー -->
	<header>
		<nav
			class="navbar navbar-fixed-top navbar-dark navbar-expand flex-md-row header-one">
			<div class="row mb-5 col-1">
				<a class="navbar-brand" href="LoginServlet">AskCommunity</a>
			</div>
			<div class="row mt-5">
				<form action="SearchResultServlet">
					<input type="search" name="search_keyword" value="${searchKeyword}">
					<input type="submit" value="検索" class="btn btn-white">
				</form>
			</div>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link text-white"
					href="QuestionPostServlet">質問する</a></li>
				<li class="nav-item"><a class="nav-link text-white"
					href="QuestionHistoryServlet">質問履歴</a></li>
				<li class="nav-item"><a class="nav-link text-white"
					href="LogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>


	<!--   検索結果   -->
	<h4 class="ml-5 mt-3">${questionCount}件の質問が見つかりました</h4>
	<h4 class="ml-5">
		<a href="SearchStatusResultServlet?status_id=1" class="text-primary"
			id="open">回答受付中</a>${openQuestionCount}件 <a
			href="SearchStatusResultServlet?status_id=2" class="text-primary"
			id="close">解決済み</a>${closeQuestionCount}件
	</h4>



	<!-- 質問リストとページネーション(「回答受付中」と「解決済み」をクリックしていない時に表示される) -->
	<table width="600" class="mt-5 mx-auto">
		<tbody>
			<!-- 1ページに質問を10件まで表示 -->
			<c:forEach var="question" items="${questionList}">
				<tr>
					<th><a href="QuestionDetailServlet?question_id=${question.id}"
						class="h3 text-primary">${question.title}</a></th>
				</tr>
				<tr>
					<td>
					<!-- 質問文は先頭から20文字のみ表示 -->
					<c:choose>
					<c:when test="${fn:length(question.question) > 20}">
    					${fn:substring(question.question, 0, 20)}・・・
  					</c:when>
					<c:otherwise>
    					${question.question}
  					</c:otherwise>
					</c:choose>
					</td>
				</tr>
				<tr>
					<td class="pb-3"><span class="mt-2 text-danger"> <c:if
								test="${question.statusId==1}">
                        回答受付中
                    </c:if>
					</span> <span class="mt-2 text-danger"> <c:if
								test="${question.statusId==2}">
                        解決済み
                    </c:if>
					</span> <span class="ml-5"> 投稿日時<fmt:formatDate
								value="${question.createDate}" pattern="yyyy/MM/dd HH:mm" />
					</span></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<!--Bootstrapでのページネーション処理-->
	<ul class="pagination justify-content-center mt-5 ml-5">
		<c:forEach var="i" begin="1" end="${maxPageNumber}">
			<li class="page-item ${i == currentPageNumber ? 'active' : ''}"
				aria-current="page"><a class="page-link"
				href="SearchPaginationServlet?max_page_Number=${maxPageNumber}&display_method=${displayMethod}&page_number=${i}">${i}</a>
			</li>
		</c:forEach>
	</ul>
	</div>



	</nav>
</body>

</html>