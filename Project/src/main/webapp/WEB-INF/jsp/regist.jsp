<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>regist</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-fixed-top navbar-dark navbar-expand flex-md-row header-one">
            <div class="row mb-5 col-10">
                <a class="navbar-brand" href="LoginServlet">AskCommunity</a>
            </div>
        </nav>
    </header>

	<!-- エラー処理 start -->
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger text-center" role="alert">${errMsg}</div>
	</c:if>
	<!--エラー処理 end  -->

    <div class="container mt-5">
        <div class="col-2 mb-5 mx-auto">
            <h3 class="title">ユーザ登録</h3>
        </div>
		
		<!--   登録フォーム   -->
		<form action="RegistConfirmServlet" method="POST">
			<div class="col-5 mx-auto">
				<table width="600">
					<tbody>
						<tr height="80">
							<td class="center h4">ユーザ名</td>
							<td class="center"><input class="form-control-underline"
								type="text" name="user_name" value="${registUser.name}"></td>
						</tr>
						<tr height="80">
							<td class="center h4">メールアドレス</td>
							<td class="center"><input class="form-control-underline"
								type="text" name="email" value="${registUser.email}"></td>
						</tr>
						<tr height="80">
							<td class="center h4">パスワード</td>
							<td class="center"><input class="form-control-underline"
								type="password" name="password"></td>
						</tr>
						<tr height="80">
							<td class="center h4">パスワード(確認用)</td>
							<td class="center"><input class="form-control-underline"
								type="password" name="password_confirm"></td>
						</tr>
					</tbody>
				</table>
				<div class="col-7 mx-auto mt-3">
					<input type="submit" value="確認" class="btn btn-primary btn-block">
				</div>
			</div>
		</form>
	</div>
</body>
</html>