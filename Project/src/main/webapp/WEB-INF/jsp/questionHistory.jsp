<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>questionHistory</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!--Material Iconsの読み込み-->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />


</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav
			class="navbar navbar-fixed-top navbar-dark navbar-expand flex-md-row header-one">
			<div class="row mb-5 col-1">
				<a class="navbar-brand" href="LoginServlet">AskCommunity</a>
			</div>
			<div class="row mt-5">
				<form action="SearchResultServlet">
					<input type="search" name="search_keyword"> <input type="submit" value="検索"
						class="btn btn-white">
				</form>
			</div>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link text-white"
					href="QuestionPostServlet">質問する</a></li>
				<li class="nav-item"><a class="nav-link text-white"
					href="QuestionHistoryServlet">質問履歴</a></li>
				<li class="nav-item"><a class="nav-link text-white"
					href="LogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>

	
	<div class="row">
		<div class="col-12 mx-auto">
			<h4 class="text-center mt-3 mb-3">${loginUser.name}さんの今までの質問</h4>

			<form action="QuestionHistoryServlet" method="POST">
				<table border="2" width="900" class="text-center mx-auto">
					<tbody>
						<tr height="60">
							<th width="300">タイトル</th>
							<th width="300">投稿日時</th>
							<th width="300">質問ステータス</th>
						</tr>
						<c:forEach var="question" items="${questionList}" varStatus="status">
						<tr height="60">
							<td width="300"><a href="QuestionDetailServlet?question_id=${question.id}"
								class="text-primary">${question.title}</a></td>
							<td width="300"><fmt:formatDate
									value="${question.createDate}" pattern="yyyy/MM/dd HH:mm" /></td>
							<td width="300"><select name="status_id_after" onchange="handleSelectChange(event)">
									<c:if test="${question.statusId == 1}">
										<option value="1" selected>回答受付中</option>
										<option value="2">解決済み</option>
									</c:if>
									<c:if test="${question.statusId == 2}">
										<option value="1">回答受付中</option>
										<option value="2" selected>解決済み</option>
									</c:if>
							</select></td>
								<input type="hidden" name="question_id" value="${question.id}">
								<input type="hidden" name="status_id_before" value="${question.statusId}">
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div class="row">
					<div class="col-9 text-right mt-5">
						<input type="submit" value="ステータスを変更する" class="btn btn-primary">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>

</html>