<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>questionDetail</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!--Material Iconsの読み込み-->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav
			class="navbar navbar-fixed-top navbar-dark navbar-expand flex-md-row header-one">
			<div class="row mb-5 col-1">
				<a class="navbar-brand" href="LoginServlet">AskCommunity</a>
			</div>
			<div class="row mt-5">
				<form action="SearchResultServlet" method="GET">
					<input type="search" name="search_keyword"> <input type="submit" value="検索"
						class="btn btn-white">
				</form>
			</div>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link text-white"
					href="QuestionPostServlet">質問する</a></li>
				<li class="nav-item"><a class="nav-link text-white"
					href="QuestionHistoryServlet">質問履歴</a></li>
				<li class="nav-item"><a class="nav-link text-white"
					href="LogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>

	<div class="row">
		<div class="col-12 mx-auto">
			<div class="row">
				<!-- 質問者以外アクセス時に表示!-->
				<c:if test="${loginUser.name != questionInfo.questionUserName}">
					<div class="col-5 mx-auto h4 mt-2 text-danger">${questionInfo.status}</div>
				</c:if>
				<!--質問者アクセス時に表示 -->
				<c:if test="${loginUser.name == questionInfo.questionUserName}">
					<div
						class="col-5 mx-auto h4 mt-2 d-flex justify-content-between align-items-center">
						<form action="QuestionDetailServlet" method="POST"
							class="d-flex flex-column col-4">

							<select name="status_id" class="mb-2">
								<c:if test="${questionInfo.statusId == 1}">
									<option value="1" selected>回答受付中</option>
									<option value="2">解決済み</option>
								</c:if>
								<c:if test="${questionInfo.statusId == 2}">
									<option value="1">回答受付中</option>
									<option value="2" selected>解決済み</option>
								</c:if>
							</select> <input type="hidden" name="question_id"
								value="${questionInfo.questionId}"> <input type="submit"
								value="ステータス変更" class="btn btn-primary">
						</form>
						<form action="QuestionDeleteConfirmServlet" method="GET">
							<input type="hidden" name="question_id"
								value="${questionInfo.questionId}">
							<input type="submit" value="質問を削除" class="btn btn-primary">
						</form>
					</div>
				</c:if>
			</div>
			<div class="text-center h2">${questionInfo.title}</div>
			<div class="row">
				<div class="col-4 mx-auto text-right mt-2">
					<fmt:formatDate value="${questionInfo.questionCreateDate}"
						pattern="yyyy/MM/dd HH:mm" />
				</div>
			</div>
			<div class="row">
				<div class="col-5 mx-auto ml-1">
					<span class="h5">${questionInfo.question}</span>
					<div class="text-right mt-2 h5">${questionInfo.questionUserName}さんより</div>
					<!-- 質問者以外アクセス時に表示!-->
					<c:if test="${loginUser.name != questionInfo.questionUserName}">
						<div class="text-center mt-5">
							<form action="AnswerPostServlet" method="GET">
								<input type="submit" value="回答する" class="btn btn-primary">
							</form>
						</div>
					</c:if>


					<div class="mt-5 h4">回答(${answerCount}件)</div>

					<c:forEach var="answer" items="${answerList}" varStatus="status">
						<div class="d-flex mt-4 ml-3 justify-content-between">
							<span class="h5">${answer.answerUserName}さん</span> <span>
								<fmt:formatDate value="${answer.answerCreateDate}"
									pattern="yyyy/MM/dd HH:mm" /> <!--投稿者自身のみ投稿を削除可能 --> <c:if
									test="${loginUser.name == answer.answerUserName}">
									<form action="AnswerDeleteConfirmServlet" method="GET"
										class="d-inline-block">
										<input type="hidden" name="answer_id"
											value="${answer.answerId}"> <input type="submit"
											value="削除" class="btn btn-primary">
									</form>
								</c:if>
							</span>
						</div>
						<div class="ml-5">
							${answer.answer}

							<!-- countはループ回数 -->
							<c:set var="count" value="0" />
							<c:forEach var="comment" items="${commentList[status.index]}">
								<div class="ml-3 mt-2">
									<span class="d-flex"> <span
										class="material-symbols-outlined">
											subdirectory_arrow_right </span> <span class="h5">
											${comment.commentUserName}さん </span> <span class="ml-auto"> <fmt:formatDate
												value="${comment.commentCreateDate}"
												pattern="yyyy/MM/dd HH:mm" /> <!--投稿者自身のみ投稿を削除可能 --> <c:if
												test="${loginUser.name == comment.commentUserName}">
												<form action="CommentDeleteConfirmServlet" method="GET"
													class="d-inline-block">
													<input type="hidden" name="comment_id"
														value="${comment.commentId}"> <input type="submit"
														value="削除" class="btn btn-primary">
												</form>
											</c:if>
									</span>
									</span>
									<div class="ml-5">${comment.comment}
										<c:set var="count" value="${count + 1}" />
							</c:forEach>
							<div class="ml-3 mt-2">
								<span class="d-flex"> <span
									class="material-symbols-outlined">
										subdirectory_arrow_right </span>
									<form action="CommentPostServlet" method="POST">
										<input type="hidden" name="answer_id"
											value="${answer.answerId}"> <input type="text"
											name="comment" placeholder="コメントを入力" required> <input
											type="submit" value="送信" class="btn btn-white">
									</form>
								</span>
							</div>
						</div>

						<!--  ループ毎にdivの終了タグを二つ追加 -->
						<c:if test="${count != 0}">
							<c:forEach begin="1" end="${count}" step="1">
				</div>
			</div>
			</c:forEach>
			</c:if>



			<hr class="my-4">
			</c:forEach>
		</div>
	</div>
	</div>
	</div>
</body>

</html>