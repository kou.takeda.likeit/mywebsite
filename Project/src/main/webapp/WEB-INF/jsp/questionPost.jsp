<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>questionPost</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- questionPost.cssの読み込み -->
    <link href="css/questionPost.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-fixed-top navbar-dark navbar-expand flex-md-row header-one">
            <ul class="navbar-nav">
                <li class="nav-item">
                    
                    <a class="nav-link text-white" href="javascript:history.back()">閉じる</a>
                </li>
            </ul>
            <span class="navbar-text mx-auto text-white h3">質問投稿</span>
        </nav>
    </header>


	<!-- エラー処理 start -->
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger text-center" role="alert">${errMsg}</div>
	</c:if>
	<!--エラー処理 end  -->

    <div class="container mt-5">
        <!--   質問投稿フォーム   -->
        <form action="QuestionPostServlet" method="POST">
            <div class="col-5 mx-auto">
                <div class="col-3">
                    <input type="text" class="title" placeholder="質問タイトル" name="title" value="${title}">
                </div>
                <div class="col-3">
                    <textarea class="question" placeholder="質問内容を入力してください" name="question">${question}</textarea>
                </div>
                <div class="col-6 mx-auto mt-4">
                	<input type="hidden" name="user_id" value="${loginUser.id}">
                    <input type="submit" value="投稿する" class="btn btn-primary btn-block">
                </div>
            </div>
        </form>
    </div>
</body>

</html>