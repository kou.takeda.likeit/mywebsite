<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>registConfirm</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-fixed-top navbar-dark navbar-expand flex-md-row header-one">
            <div class="row mb-5 col-10">
                <a class="navbar-brand" href="LoginServlet">AskCommunity</a>
            </div>
        </nav>
    </header>


    <div class="container mt-5">
        <div class="row col-3 mb-5 mx-auto">
            <h3>入力内容確認</h3> 
        </div>
        <div class="col-5 mx-auto mb-2">
            <table width="550">
                <tbody>
                    <tr height="80">
                        <td class="center h4">ユーザ名</td>
                        <td class="center">${registUser.name}</td>
                    </tr>
                    <tr height="80">
                        <td class="center h4">メールアドレス</td>
                        <td class="center">${registUser.email}</td>
                    </tr>
                    <tr height="80">
                        <td class="center h4">パスワード</td>
                        <td class="center">${registUser.password}</td>
                    </tr>
                </tbody>
            </table>
        </div>


        <div class="d-flex justify-content-center">
            <div class="mr-5">
                <form action="RegistServlet" method="POST">
                    <input type="submit" value="修正" class="btn btn-primary">
                </form>
            </div>
            <div class="ml-5">
                <form action="RegistResultServlet" method="GET">
                    <input type="submit" value="登録" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
</body>
</html>