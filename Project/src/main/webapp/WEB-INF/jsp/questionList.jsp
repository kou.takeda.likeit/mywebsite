<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>questionList</title>



<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />

<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />

<!-- questionList.cssの読み込み -->
<link href="css/questionList.css" rel="stylesheet" type="text/css" />

<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">


</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav
			class="navbar navbar-fixed-top navbar-dark navbar-expand flex-md-row header-one">
			<div class="row mb-5 col-1">
				<a class="navbar-brand" href="LoginServlet">AskCommunity</a>
			</div>
			<div class="row mt-5">
				<form action="SearchResultServlet">
					<input type="search" name="search_keyword"> <input type="submit" value="検索"
						class="btn btn-white">
				</form>
			</div>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link text-white"
					href="QuestionPostServlet">質問する</a></li>
				<li class="nav-item"><a class="nav-link text-white"
					href="QuestionHistoryServlet">質問履歴</a></li>
				<li class="nav-item"><a class="nav-link text-white"
					href="LogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>


	<!--   質問一覧テーブル   -->
	<div class="row col-5 mt-5 mx-auto">
		<table border="2" width=100%>
			<tbody>
				<tr height="60">
					<form method="POST" action="QuestionListServlet">
						<th class="text-center h3 <c:if test='${highLightStatus == "open"}'>table-primary</c:if>" width=50%><input
							type="hidden" name="status_id" value="1"> <input
							type="submit" value="回答受付中" id="open"
							class="btn-light bg-transparent border-0 d-block mx-auto"></th>
					</form>
					<form method="POST" action="QuestionListServlet">
						<th class="text-center h3 <c:if test='${highLightStatus == "close"}'>table-primary</c:if>" width=50%><input
							type="hidden" name="status_id" value="2"> <input
							type="submit" value="解決済みの質問"
							class="btn-light bg-transparent border-0 d-block mx-auto"></th>
					</form>
				</tr>
			

				<c:forEach var="question" items="${questionList}">
					<tr height="30">
						<th colspan="2" class="title">
							<div class="text-center">
								<a href="QuestionDetailServlet?question_id=${question.id}" class="h3 text-primary">${question.title}</a>
							</div>
						</th>
					</tr>
					<tr>
						<td colspan="2" class="time">
							<div class="text-right">
								<fmt:formatDate value="${question.createDate}"
									pattern="yyyy/MM/dd HH:mm" />

							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>

</html>