<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>registResult</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-fixed-top navbar-dark navbar-expand flex-md-row header-one">
            <div class="row mb-5 col-10">
                <a class="navbar-brand" href="LoginServlet">AskCommunity</a>
            </div>
        </nav>
    </header>


    <div class="container mt-5">
        <div class="row col-2 mb-5 mx-auto">
            <h2 class="mt-5">登録完了</h3>
        </div>
        <div class="row col-4 mb-5 mx-auto">
            <h4 class="mt-4">ご登録ありがとうございます。さっそくログインして質問したり、質問を閲覧したりしてみましょう。</h4>
        </div>
    </div>


    <div class="d-flex justify-content-center">
        <form action="LoginServlet" method="GET">
            <div class="mx-auto">
                <input type="submit" value="ログインする" class="btn btn-primary ">
            </div>
        </form>
    </div>
   
</body>

</html>