<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
<!-- login.cssの読み込み -->
<link href="css/login.css" rel="stylesheet" type="text/css" />
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav
			class="navbar navbar-fixed-top navbar-dark navbar-expand flex-md-row header-one">
			<div class="row mb-5 col-10">
				<a class="navbar-brand" href="LoginServlet">AskCommunity</a>
			</div>
		</nav>
	</header>

	<!-- エラー処理 start -->
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger text-center" role="alert">${errMsg}</div>
	</c:if>
	<!--エラー処理 end  -->
	
	<!-- ログアウト処理 start -->
	<c:if test="${isLogout == true}">
		<div class="alert alert-success text-center" role="alert">ログアウトしました</div>
	</c:if>
	<!--ログアウト処理 end  -->


	<!-- ログインフォーム -->
	<form class="form-signin mt-5" action="LoginServlet" method="POST">
		<div class="row mb-4 col-10 mx-auto">
			<label class="h4 mb-4">メールアドレス</label> <input
				class="form-control-underline" type="text" name="email" value="${email}">
		</div>
		<div class="row mb-5 mt-5 col-10 mx-auto">
			<label class="h4 mb-4">パスワード</label> <input
				class="form-control-underline" type="password" name="password">
		</div>
		<div class="row col-10 mx-auto">
			<input type="submit" value="ログイン" class="btn btn-primary btn-block">
		</div>
	</form>

	<div class="row col-7 ml-5 justify-content-end">
		<a href="RegistServlet" class="regist">新規登録</a>
	</div>

</body>

</html>