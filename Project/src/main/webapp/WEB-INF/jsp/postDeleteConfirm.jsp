<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>postDeleteConfirm</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- postDeleteConfirm.cssの読み込み -->
    <link href="css/postDeleteConfirm.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-fixed-top navbar-dark navbar-expand flex-md-row header-one">
            <ul class="navbar-nav">
                <li class="nav-item">
                   
                    <a class="nav-link text-white" href="javascript:history.back()">閉じる</a>
                </li>
            </ul>
            <span class="navbar-text mx-auto text-white h3">投稿削除確認</span>
        </nav>
    </header>


    <div class="container mt-5">
		<div class="col-5 mx-auto">
			<div class="scrollable mb-5">
				<p class="text-center">
				<c:if test="${answer != null}">
					${answer}
				</c:if>
				<c:if test="${comment != null}">
					${comment}
				</c:if>
				</p>
			</div>
			<p class="col-8 ml-auto mb-4">削除してよろしいですか？</p>
			<div class="d-flex justify-content-center">
			
				<form action="QuestionDetailServlet" method="GET">
					<div class="ml-5">
						<input type="hidden" name="question_id" value="${questionInfo.questionId}">
						<input type="submit" value="キャンセル" class="btn btn-primary ">
					</div>
				</form>
				
				<c:if test="${answerId != null}">
				<form action="AnswerDeleteConfirmServlet" method="POST">
					<div class="ml-5">
						<input type="hidden" name="answer_id" value="${answerId}">
						<input type="submit" value="削除" class="btn btn-primary">
					</div>
				</form>
				</c:if>
				
				<c:if test="${commentId != null}">
				<form action="CommentDeleteConfirmServlet" method="POST">
					<div class="ml-5">
						<input type="hidden" name="comment_id" value="${commentId}">
						<input type="submit" value="削除" class="btn btn-primary">
					</div>
				</form>
				</c:if>
			</div>
		</div>
	</div>
</body>

</html>