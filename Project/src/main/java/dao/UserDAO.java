package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import model.User;
import util.PasswordEncorder;

// ユーザテーブル操作
public class UserDAO {

  /**
   * メールアドレスとパスワードからユーザ情報を取得する：DB検索処理
   */
  public User findByLoginInfo(String email, String encorderdPassword) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE email = ? and password = ?";


      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, email);
      pStmt.setString(2, encorderdPassword);
      ResultSet rs = pStmt.executeQuery();

      // レコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String name = rs.getString("name");
      String _email = rs.getString("email");
      String _encorderdPassword = rs.getString("password");
      Timestamp createDate = rs.getTimestamp("create_date");

      return new User(id, name, _email, _encorderdPassword, createDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }


  /**
   * ユーザ名の重複チェック
   */
  public boolean isOverlapUserName(String name) {
    // 重複していればtrue
    boolean isOverlap = false;
    Connection conn = null;
    PreparedStatement pStmt = null;

    try {
      conn = DBManager.getConnection();
      // 入力されたユーザ名が存在するか調べる
      pStmt = conn.prepareStatement("SELECT name FROM user WHERE name= ?");
      pStmt.setString(1, name);

      ResultSet rs = pStmt.executeQuery();

      if (rs.next()) {
        isOverlap = true;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return isOverlap;
  }


  /**
   * メールアドレスの重複チェック
   */
  public boolean isOverlapEmail(String email) throws SQLException {
    // 重複していればtrue
    boolean isOverlap = false;
    Connection conn = null;
    PreparedStatement pStmt = null;

    try {
      conn = DBManager.getConnection();
      // 入力されたメールアドレスが存在するか調べる
      pStmt = conn.prepareStatement("SELECT email FROM user WHERE email= ?");
      pStmt.setString(1, email);

      ResultSet rs = pStmt.executeQuery();

      if (rs.next()) {
        isOverlap = true;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return isOverlap;
  }

  // ユーザ新規登録
  public void insert(User registUser) {
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "INSERT INTO user (name, email, password, create_date)VALUES(?,?,?,now())";

      String encorderdPassword = PasswordEncorder.encordPassword(registUser.getPassword());

      stmt = conn.prepareStatement(sql);
      stmt.setString(1, registUser.getName());
      stmt.setString(2, registUser.getEmail());
      stmt.setString(3, encorderdPassword);

      // SQL実行
      stmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



}
