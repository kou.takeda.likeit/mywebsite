package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.Question;
import model.QuestionAndUserAndStatus;

// 質問テーブル操作
public class QuestionDAO {


  /**
   * 引数に指定した件数分、質問情報を取得する
   */
  public List<Question> findQuestions(int count) {
    Connection conn = null;
    List<Question> questionList = new ArrayList<Question>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM question ORDER BY create_date DESC LIMIT ?";

      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, count);

      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        int userId = rs.getInt("user_id");
        String title = rs.getString("title");
        String question = rs.getString("question");
        Timestamp createDate = rs.getTimestamp("create_date");
        int statusId = rs.getInt("status_id");

        Question questionInfo = new Question(id, userId, title, question, createDate, statusId);

        questionList.add(questionInfo);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return questionList;
  }



  /**
   * 指定された質問ステータスIDを持つ質問情報をcountの値分、取得する
   */
  public List<Question> findQuestionsByStatusId(int statusId, int count) {
    Connection conn = null;
    List<Question> questionList = new ArrayList<Question>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM question WHERE status_id=? ORDER BY create_date DESC LIMIT ?";

      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, statusId);
      pStmt.setInt(2, count);

      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        int userId = rs.getInt("user_id");
        String title = rs.getString("title");
        String question = rs.getString("question");
        Timestamp createDate = rs.getTimestamp("create_date");
        int _statusId = rs.getInt("status_id");

        Question questionInfo = new Question(id, userId, title, question, createDate, _statusId);

        questionList.add(questionInfo);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return questionList;
  }


  // 質問登録
  public void insert(Question questionInfo) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "INSERT INTO question(user_id,title,question,create_date)VALUES(?,?,?,now())";


      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, questionInfo.getUserId());
      pStmt.setString(2, questionInfo.getTitle());
      pStmt.setString(3, questionInfo.getQuestion());

      // SQL実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }


  // 質問IDによる質問情報検索(質問ユーザ情報と質問ステータス情報を含む)
  public QuestionAndUserAndStatus findQuestionDetailByQuestionId(int questionId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "SELECT question.id,question.user_id,question.title,question.question,question.create_date,"
              + "question.status_id,question_status.status,user.name,user.email,user.password,user.create_date "
              + "FROM question INNER JOIN question_status ON question.status_id=question_status.id "
              + "INNER JOIN user ON question.user_id=user.id WHERE question.id=?";


      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, questionId);
      ResultSet rs = pStmt.executeQuery();

      // レコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int _questionId = rs.getInt("question.id");
      int questionUserId = rs.getInt("question.user_id");
      String title = rs.getString("question.title");
      String question = rs.getString("question.question");
      Timestamp questionCreateDate = rs.getTimestamp("question.create_date");
      int statusId = rs.getInt("question.status_id");
      String status = rs.getString("question_status.status");
      String questionUserName = rs.getString("user.name");
      String questionUserEmail = rs.getString("user.email");
      String questionUserPassword = rs.getString("user.password");
      Timestamp questionUserCreateDate = rs.getTimestamp("user.create_Date");

      return new QuestionAndUserAndStatus(_questionId, questionUserId, title, question,
          questionCreateDate, statusId, status, questionUserName, questionUserEmail,
          questionUserPassword, questionUserCreateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // ユーザIDによる質問情報検索
  public List<Question> findQuestionsByUserId(int userId) {
    Connection conn = null;
    List<Question> questionList = new ArrayList<Question>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM question WHERE user_id=? ORDER BY create_date DESC";

      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, userId);

      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        int _userId = rs.getInt("user_id");
        String title = rs.getString("title");
        String question = rs.getString("question");
        Timestamp createDate = rs.getTimestamp("create_date");
        int statusId = rs.getInt("status_id");

        Question questionInfo = new Question(id, _userId, title, question, createDate, statusId);

        questionList.add(questionInfo);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return questionList;
  }



  // 引数の質問IDに該当する質問の質問ステータスIDを変更する。
  public void updateStatus(int statusId, int questionId) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "UPDATE question SET status_id=? WHERE id=?";


      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, statusId);
      pStmt.setInt(2, questionId);

      // SQL実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }


  // 質問IDに紐づく質問情報の取得
  public Question findQuestionByQuestionId(int questionId) {

    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM question WHERE id = ?";


      // SELECT文を実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, questionId);

      ResultSet rs = pStmt.executeQuery();

      // レコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      int userId = rs.getInt("user_id");
      String title = rs.getString("title");
      String question = rs.getString("question");
      Timestamp createDate = rs.getTimestamp("create_date");
      int statusId = rs.getInt("status_id");

      return new Question(id, userId, title, question, createDate, statusId);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }


  // 質問IDに紐づく質問情報を削除する。
  public void deleteByQuestionId(int questionId) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM question WHERE id=?";

      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, questionId);

      // SQL実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



  /*
   * 指定された質問タイトルを含む質問情報を投稿日時が新しい順に並び替えた後、先頭からindex+1番目の行からcountの値分、取得する
   */

  public List<Question> findQuestionsByTitle(String title, int count, int index) {
    Connection conn = null;
    List<Question> questionList = new ArrayList<Question>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql =
          "SELECT * FROM question WHERE title LIKE ? ORDER BY create_date DESC LIMIT ? OFFSET ?";

      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, "%" + title + "%");
      pStmt.setInt(2, count);
      pStmt.setInt(3, index);

      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        int userId = rs.getInt("user_id");
        String _title = rs.getString("title");
        String question = rs.getString("question");
        Timestamp createDate = rs.getTimestamp("create_date");
        int _statusId = rs.getInt("status_id");

        Question questionInfo = new Question(id, userId, _title, question, createDate, _statusId);

        questionList.add(questionInfo);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return questionList;
  }


  // 指定された質問タイトルを含む質問情報の個数を取得する
  public int countQuestionsByTitle(String title) {
    Connection conn = null;

    int count = 0;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT COUNT(*) FROM question WHERE title LIKE ?";

      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, "%" + title + "%");

      ResultSet rs = pStmt.executeQuery();

      // レコードは1件のみなので、rs.next()は1回だけ行う
      rs.next();

      // 質問情報の個数を取得
      count = rs.getInt(1);


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return count;
  }

  /**
   * 指定された質問タイトルを含み、かつ、指定された質問ステータスIDを持つ質問情報を投稿日時が新しい順に並び替えた後、先頭からindex+1番目の行からcountの値分、取得する
   */
  public List<Question> findQuestionsByTitleAndStatusId(String title, int statusId, int count,
      int index) {
    Connection conn = null;
    List<Question> questionList = new ArrayList<Question>();


    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql =
          "SELECT * FROM question WHERE title LIKE ? AND status_id=? ORDER BY create_date DESC LIMIT ? OFFSET ?";

      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, "%" + title + "%");
      pStmt.setInt(2, statusId);
      pStmt.setInt(3, count);
      pStmt.setInt(4, index);


      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        int userId = rs.getInt("user_id");
        String _title = rs.getString("title");
        String question = rs.getString("question");
        Timestamp createDate = rs.getTimestamp("create_date");
        int _statusId = rs.getInt("status_id");

        Question questionInfo = new Question(id, userId, _title, question, createDate, _statusId);

        questionList.add(questionInfo);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return questionList;
  }



  /**
   * 指定された質問タイトルを含み、かつ、指定された質問ステータスIDを持つ質問情報の個数を取得する
   */
  public int countQuestionsByTitleAndStatusId(String title, int statusId) {
    Connection conn = null;
    int count = 0;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql =
          "SELECT COUNT(*) FROM question WHERE title LIKE ? AND status_id=?";

      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, "%" + title + "%");
      pStmt.setInt(2, statusId);

      ResultSet rs = pStmt.executeQuery();

      // レコードは1件のみなので、rs.next()は1回だけ行う
      rs.next();

      // 質問情報の個数を取得
      count = rs.getInt(1);

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return count;
  }



}
