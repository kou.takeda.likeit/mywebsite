package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.Answer;
import model.AnswerAndUser;

public class AnswerDAO {

  // 質問IDに紐づく回答一覧情報を取得
  public List<AnswerAndUser> findAnswers(int questionId) {
    Connection conn = null;
    List<AnswerAndUser> answerList = new ArrayList<>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql =
          "SELECT answer.id,answer.user_id,answer.answer,answer.create_date,answer.question_id,"
              + "user.name,user.email,user.password,user.create_date FROM answer INNER JOIN user "
              + "ON answer.user_id=user.id WHERE answer.question_id=? ORDER BY answer.create_date DESC";

      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, questionId);

      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int answerId = rs.getInt("answer.id");
        int answerUserId = rs.getInt("answer.user_id");
        String answer = rs.getString("answer.answer");
        Timestamp answerCreateDate = rs.getTimestamp("answer.create_date");
        int _questionId = rs.getInt("answer.question_id");
        String answerUserName = rs.getString("user.name");
        String answerUserEmail = rs.getString("user.email");
        String answerUserPassword = rs.getString("user.password");
        Timestamp answerUserCreateDate = rs.getTimestamp("user.create_Date");

        AnswerAndUser answerInfo = new AnswerAndUser(answerId, answerUserId, answer, answerCreateDate,
            _questionId, answerUserName, answerUserEmail, answerUserPassword, answerUserCreateDate);

        answerList.add(answerInfo);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return answerList;
  }

  // answerテーブルへ回答情報を登録する
  public void insert(int loginUserId, String answer, int questionId) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "INSERT INTO answer(user_id,answer,create_date,question_id)VALUES(?,?,now(),?)";


      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, loginUserId);
      pStmt.setString(2, answer);
      pStmt.setInt(3, questionId);

      // SQL実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // 回答IDに紐づく回答情報の取得
  public Answer findAnswerByAnswerId(int answerId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM answer WHERE id = ?";


      // SELECT文を実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, answerId);

      ResultSet rs = pStmt.executeQuery();

      // レコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      int userId = rs.getInt("user_id");
      String answer = rs.getString("answer");
      Timestamp createDate = rs.getTimestamp("create_date");
      int questionId = rs.getInt("question_id");

      return new Answer(id, userId, answer, createDate, questionId);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }


  // 回答IDに紐づく回答情報を削除する。
  public void deleteByAnswerId(int answerId) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM answer WHERE id=?";

      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, answerId);

      // SQL実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // 質問IDに紐づく回答情報を全て削除する
  public void deleteByQuestionId(int questionId) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM answer WHERE question_id=?";

      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, questionId);

      // SQL実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }


}
