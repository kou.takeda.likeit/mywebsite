package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.QuestionStatus;

public class QuestionStatusDAO {

  public QuestionStatus findStatus(int statusId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM question_status WHERE id=?";


      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, statusId);
      ResultSet rs = pStmt.executeQuery();

      // レコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String status = rs.getString("status");

      return new QuestionStatus(id, status);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

}
