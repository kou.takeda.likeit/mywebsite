package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.Comment;
import model.CommentAndUser;


public class CommentDAO {

  // 回答IDに紐づくコメント一覧情報を取得
  public List<CommentAndUser> findCommentList(int answerId) {
    Connection conn = null;
    List<CommentAndUser> commentList = new ArrayList<>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql =
          "SELECT comment.id,comment.user_id,comment.comment,comment.create_date,comment.question_id,comment.answer_id,"
              + "user.name,user.email,user.password,user.create_date FROM comment "
              + "INNER JOIN user ON comment.user_id=user.id WHERE answer_id=? ORDER BY comment.create_date ASC";

      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, answerId);

      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int commentId = rs.getInt("comment.id");
        int commentUserId = rs.getInt("comment.user_id");
        String comment = rs.getString("comment.comment");
        Timestamp commentCreateDate = rs.getTimestamp("comment.create_date");
        int questionId = rs.getInt("comment.question_id");
        int _answerId = rs.getInt("comment.answer_id");
        String commentUserName = rs.getString("user.name");
        String commentUserEmail = rs.getString("user.email");
        String commentUserPassword = rs.getString("user.password");
        Timestamp commentUserCreateDate = rs.getTimestamp("user.create_Date");

        CommentAndUser commentInfo = new CommentAndUser(commentId, commentUserId, comment,
            commentCreateDate, questionId, _answerId, commentUserName, commentUserEmail,
            commentUserPassword, commentUserCreateDate);

        commentList.add(commentInfo);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return commentList;
  }


  // commentテーブルへコメント情報を登録する
  public void insert(int loginUserId, String comment, int questionId, int answerId) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql =
          "INSERT INTO comment(user_id,comment,create_date,question_id,answer_id)VALUES(?,?,now(),?,?)";


      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, loginUserId);
      pStmt.setString(2, comment);
      pStmt.setInt(3, questionId);
      pStmt.setInt(4, answerId);

      // SQL実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // コメントIDに紐づくコメント情報の取得
  public Comment findCommentByCommentId(int commentId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM comment WHERE id = ?";

      // SELECT文を実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, commentId);

      ResultSet rs = pStmt.executeQuery();

      // レコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      int userId = rs.getInt("user_id");
      String comment = rs.getString("comment");
      Timestamp createDate = rs.getTimestamp("create_date");
      int questionId = rs.getInt("question_id");
      int answerId = rs.getInt("answer_id");

      return new Comment(id, userId, comment, createDate, questionId, answerId);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // 回答IDに紐づくコメント情報を全て削除する
  public void deleteByAnswerId(int answerId) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM comment WHERE answer_id=?";

      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, answerId);

      // SQL実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // コメントIDに紐づくコメント情報を削除する
  public void deleteByCommentId(int commentId) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM comment WHERE id=?";

      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, commentId);

      // SQL実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // 質問IDに紐づくコメント情報を全て削除する
  public void deleteByQuestionId(int questionId) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM comment WHERE question_id=?";

      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, questionId);

      // SQL実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }


}
