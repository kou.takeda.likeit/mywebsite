package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.CommentDAO;
import model.Comment;
import model.QuestionAndUserAndStatus;
import model.User;

/**
 * Servlet implementation class CommentDeleteConfirmServlet
 */
@WebServlet("/CommentDeleteConfirmServlet")
public class CommentDeleteConfirmServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CommentDeleteConfirmServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // ログイン情報が存在しない場合、LoginServletへリダイレクト
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    /* ログイン情報が存在する場合 */
    try {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // 削除するコメントのIDを取得
      String commentId = request.getParameter("comment_id");

      CommentDAO commentDao = new CommentDAO();

      // 削除対象のコメント情報を取得
      Comment commentInfo = commentDao.findCommentByCommentId(Integer.parseInt(commentId));

      // 削除対象のコメント文を取得
      String comment = commentInfo.getComment();

      request.setAttribute("commentId", commentId);
      request.setAttribute("comment", comment);

      // postDeleteConfirm.jspへフォワード
      RequestDispatcher dispatcher =
          request.getRequestDispatcher("/WEB-INF/jsp/postDeleteConfirm.jsp");
      dispatcher.forward(request, response);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // 削除するコメントのIDを取得
    int commentId = Integer.parseInt(request.getParameter("comment_id"));

    HttpSession session = request.getSession();

    try {
      CommentDAO commentDao = new CommentDAO();

      // commentテーブルより削除対象のコメントIDを持つレコードを削除する。
      commentDao.deleteByCommentId(commentId);


      // クリックされた質問の情報を取得
      QuestionAndUserAndStatus questionInfo =
          (QuestionAndUserAndStatus) session.getAttribute("questionInfo");

      // クリックされた質問の質問IDを取得
      int questionId = questionInfo.getQuestionId();

      // QuestionDetailServletにリダイレクト
      response.sendRedirect("QuestionDetailServlet?question_id=" + questionId);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}