package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.QuestionDAO;
import model.Question;
import model.User;

/**
 * Servlet implementation class QuestionPostServlet
 */
@WebServlet("/QuestionPostServlet")
public class QuestionPostServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public QuestionPostServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // ログイン情報が存在しない場合、LoginServletへリダイレクト
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    // ログイン情報が存在する場合、questionPost.jspへフォワード */

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/questionPost.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");
    
    String title = request.getParameter("title");
    String question=request.getParameter("question");
    String userId = request.getParameter("user_id");
    

    
    // 質問タイトル、または質問文が未入力の場合、エラーとする
    if (title.equals("")||question.equals("")) {
      String errMsg = "未入力の項目があります";
      
      request.setAttribute("errMsg", errMsg);
      request.setAttribute("title", title);
      request.setAttribute("question", question);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/questionPost.jsp");
      dispatcher.forward(request, response);
      return;
    }



    // 質問タイトルと質問文が入力されている場合、質問情報を登録する。
    try {
      QuestionDAO questionDao = new QuestionDAO();

      Question questionInfo = new Question();
      questionInfo.setTitle(title);
      questionInfo.setQuestion(question);
      questionInfo.setUserId(Integer.parseInt(userId));

      // 質問登録処理
      questionDao.insert(questionInfo);

      // QuestionListServletにリダイレクト
      response.sendRedirect("QuestionListServlet");

    } catch (Exception e) {
      e.printStackTrace();
    }
    
  }

}
