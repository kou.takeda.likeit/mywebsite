package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDAO;
import model.User;

/**
 * Servlet implementation class RegistResultServlet
 */
@WebServlet("/RegistResultServlet")
public class RegistResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistResultServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      HttpSession session = request.getSession();


      try {
        // 新規登録処理
        User registUser = (User) session.getAttribute("registUser");
        UserDAO userDao = new UserDAO();
        userDao.insert(registUser);

        // セッションに保存されている登録ユーザ情報を削除する
        session.removeAttribute("registUser");

        // registResult.jspにフォワード
        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/registResult.jsp");
        dispatcher.forward(request, response);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
