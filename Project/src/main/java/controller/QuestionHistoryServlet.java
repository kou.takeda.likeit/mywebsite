package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.QuestionDAO;
import model.Question;
import model.User;

/**
 * Servlet implementation class QuestionHistoryServlet
 */
@WebServlet("/QuestionHistoryServlet")
public class QuestionHistoryServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public QuestionHistoryServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // ログイン情報が存在しない場合、LoginServletへリダイレクト
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }


    /* ログイン情報が存在する場合 */
    try {

      // ログインユーザのIDを取得
      int loginUserId = loginUser.getId();

      QuestionDAO questionDao = new QuestionDAO();

      // ログインユーザの質問情報を取得
      List<Question> questionList = questionDao.findQuestionsByUserId(loginUserId);

      request.setAttribute("questionList", questionList);



      // questionHistory.jspへフォワード
      RequestDispatcher dispatcher =
          request.getRequestDispatcher("/WEB-INF/jsp/questionHistory.jsp");
      dispatcher.forward(request, response);


    } catch (Exception e) {
      e.printStackTrace();
    }

  }



  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    try {

      // 質問ステータスID(questionHistory.jsp遷移前の値)を全て取得
      String[] beforeStatusIdArray = request.getParameterValues("status_id_before");

      // 質問ステータスID(questionHistory.jspのsubmitボタン押下時の値)を全て取得
      String[] afterStatusIdArray = request.getParameterValues("status_id_after");

      // ログインユーザの質問IDを全て取得
      String[] questionIdArray = request.getParameterValues("question_id");

      QuestionDAO questionDao = new QuestionDAO();

      for (int i = 0; i < beforeStatusIdArray.length; i++) {
        // 質問ステータスIDに変更がある場合、questionテーブルの質問ステータスIDを変更
        if (afterStatusIdArray[i].equals(beforeStatusIdArray[i]) == false) {
          questionDao.updateStatus(Integer.parseInt(afterStatusIdArray[i]),
              Integer.parseInt(questionIdArray[i]));
        }
      }

      // doGetメソッドの呼び出し
      doGet(request, response);


    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
