package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.AnswerDAO;
import dao.CommentDAO;
import dao.QuestionDAO;
import model.AnswerAndUser;
import model.CommentAndUser;
import model.QuestionAndUserAndStatus;
import model.User;

/**
 * Servlet implementation class QuestionDetailServlet
 */
@WebServlet("/QuestionDetailServlet")
public class QuestionDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuestionDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    HttpSession session = request.getSession();
	    User loginUser = (User) session.getAttribute("loginUser");

	    // ログイン情報が存在しない場合、LoginServletへリダイレクト
	    if (loginUser == null) {
	      response.sendRedirect("LoginServlet");
	      return;
	    }


	    /* ログイン情報が存在する場合 */
	    try {
	      // リクエストパラメータの文字コードを指定
	      request.setCharacterEncoding("UTF-8");
	      
	      //クリックされた質問タイトルのID(質問ID)を取得
	      int questionId=Integer.parseInt(request.getParameter("question_id"));
	      

	     
          QuestionDAO questionDao = new QuestionDAO();
          AnswerDAO answerDao = new AnswerDAO();
          CommentDAO commentDao = new CommentDAO();
          
          
          //クリックされた質問の情報を取得
          QuestionAndUserAndStatus questionInfo = questionDao.findQuestionDetailByQuestionId(questionId);
	      
          //クリックされた質問に投稿されている回答情報の一覧を取得
          List<AnswerAndUser> answerList=answerDao.findAnswers(questionId);
          
          // クリックされた質問内の各回答毎のコメント情報の一覧
          List<List<CommentAndUser>> commentList = new ArrayList<List<CommentAndUser>>();;

          // 各回答毎にコメント情報の一覧を取得し、二次元リストに追加
          for (AnswerAndUser answerUser : answerList) {
            List<CommentAndUser> innerCommentList = commentDao.findCommentList(answerUser.getAnswerId());
            commentList.add(innerCommentList);
            
          }

          // 回答数を取得
          int answerCount = answerList.size();


          session.setAttribute("questionInfo", questionInfo);
          session.setAttribute("answerList", answerList);
          session.setAttribute("commentList", commentList);
          session.setAttribute("answerCount", answerCount);
          
	      // questionDetail.jspへフォワード
	      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/questionDetail.jsp");
	      dispatcher.forward(request, response);
	      
	    
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      try {

        // 質問ステータスIDを取得
        int statusId = Integer.parseInt(request.getParameter("status_id"));

        // 質問IDを取得
        int questionId = Integer.parseInt(request.getParameter("question_id"));

        // 質問ステータス
        String status = "";

        if (statusId == 1) {
          status = "回答受付中";
        } else if (statusId == 2) {
          status = "解決済み";
        }

        HttpSession session = request.getSession();

        QuestionAndUserAndStatus questionInfo =
            (QuestionAndUserAndStatus) session.getAttribute("questionInfo");

        // インスタンスに設定されている質問ステータスIDと質問ステータスを変更
        questionInfo.setStatusId(statusId);
        questionInfo.setStatus(status);


        QuestionDAO questionDao = new QuestionDAO();

        // questionテーブルの質問ステータスIDを変更
        questionDao.updateStatus(statusId, questionId);
        
        // questionDetail.jspへフォワード
        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/questionDetail.jsp");
        dispatcher.forward(request, response);
        
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
}
