package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.AnswerDAO;
import model.QuestionAndUserAndStatus;
import model.User;

/**
 * Servlet implementation class AnswerPostServlet
 */
@WebServlet("/AnswerPostServlet")
public class AnswerPostServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public AnswerPostServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // ログイン情報が存在しない場合、LoginServletへリダイレクト
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    /* ログイン情報が存在する場合 */

    // answerPost.jspへフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/answerPost.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // 回答文を取得
    String answer = request.getParameter("answer");

    // 回答文が未入力の場合、エラーとする
    if (answer.equals("")) {
      String errMsg = "回答が未入力です";

      request.setAttribute("errMsg", errMsg);
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/answerPost.jsp");
      dispatcher.forward(request, response);
      return;
    }

    // 回答文が入力されている場合、回答情報を登録する。
    try {

      HttpSession session = request.getSession();
      // クリックされた質問の情報を取得
      QuestionAndUserAndStatus questionInfo =
          (QuestionAndUserAndStatus) session.getAttribute("questionInfo");

      // ログインユーザの情報を取得
      User loginUser = (User) session.getAttribute("loginUser");

      // クリックされた質問の質問IDを取得
      int questionId = questionInfo.getQuestionId();

      // ログインユーザのIDを取得
      int loginUserId = loginUser.getId();


      AnswerDAO answerDao = new AnswerDAO();


      // answerテーブルへ回答情報を登録する。
      answerDao.insert(loginUserId, answer, questionId);

      // QuestionDetailServletにリダイレクト
      response.sendRedirect("QuestionDetailServlet?question_id=" + questionId);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}