package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.CommentDAO;
import model.QuestionAndUserAndStatus;
import model.User;

/**
 * Servlet implementation class CommentPostServlet
 */
@WebServlet("/CommentPostServlet")
public class CommentPostServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CommentPostServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // ログイン情報が存在しない場合、LoginServletへリダイレクト
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    // コメント文を取得
    String comment = request.getParameter("comment");

    // クリックされた質問の情報を取得
    QuestionAndUserAndStatus questionInfo =
        (QuestionAndUserAndStatus) session.getAttribute("questionInfo");

    // クリックされた質問の質問IDを取得
    int questionId = questionInfo.getQuestionId();

    // コメント文が未入力の場合はコメントを登録しない
    if (comment.equals("")) {
      response.sendRedirect("QuestionDetailServlet?question_id=" + questionId);
      return;
    }

    // コメント文が入力されている場合、コメント情報を登録する。
    try {
      // ログインユーザの情報を取得
      User loginUser = (User) session.getAttribute("loginUser");

      // ログインユーザのIDを取得
      int loginUserId = loginUser.getId();

      // 回答IDを取得
      int answerId = Integer.parseInt(request.getParameter("answer_id"));

      CommentDAO commentDao = new CommentDAO();

      // commentテーブルへコメント情報を登録する。
      commentDao.insert(loginUserId, comment, questionId, answerId);

      // QuestionDetailServletにリダイレクト
      response.sendRedirect("QuestionDetailServlet?question_id=" + questionId);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}