package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.AnswerDAO;
import dao.CommentDAO;
import dao.QuestionDAO;
import model.Question;
import model.User;

/**
 * Servlet implementation class QuestionDeleteConfirmServlet
 */
@WebServlet("/QuestionDeleteConfirmServlet")
public class QuestionDeleteConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuestionDeleteConfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      User loginUser = (User) session.getAttribute("loginUser");

      // ログイン情報が存在しない場合、LoginServletへリダイレクト
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }


      /* ログイン情報が存在する場合 */
      try {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // 削除する質問のIDを取得
        String questionId = request.getParameter("question_id");

        QuestionDAO questionDao = new QuestionDAO();

        // 削除対象の質問情報を取得
        Question questionInfo = questionDao.findQuestionByQuestionId(Integer.parseInt(questionId));

        // 削除対象の質問タイトルを取得
        String title = questionInfo.getTitle();

        // 削除対象の質問文を取得
        String question = questionInfo.getQuestion();


        request.setAttribute("title", title);
        request.setAttribute("question", question);
        request.setAttribute("questionId", questionId);

        // questionDeleteConfirm.jspへフォワード
        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/questionDeleteConfirm.jsp");
        dispatcher.forward(request, response);
      } catch (Exception e) {
        e.printStackTrace();
      }

    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // 削除する質問のIDを取得
      int questionId = Integer.parseInt(request.getParameter("question_id"));

      HttpSession session = request.getSession();

      try {
        CommentDAO commentDao = new CommentDAO();
        AnswerDAO answerDao = new AnswerDAO();
        QuestionDAO questionDao = new QuestionDAO();



        // commentテーブルより削除対象の質問IDを持つレコードを全て削除する。
        commentDao.deleteByQuestionId(questionId);

        // answerテーブルより削除対象の質問IDを持つレコードを全て削除する。
        answerDao.deleteByQuestionId(questionId);

        // quesitonテーブルより削除対象の質問IDを持つレコードを削除する。
        questionDao.deleteByQuestionId(questionId);



        // QuestionListServletにリダイレクト
        response.sendRedirect("QuestionListServlet");

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
