package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.QuestionDAO;
import model.Question;
import model.User;

/**
 * Servlet implementation class QuestionStatusServlet
 */
@WebServlet("/SearchStatusResultServlet")
public class SearchStatusResultServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public SearchStatusResultServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // ログイン情報が存在しない場合、LoginServletへリダイレクト
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    /* ログイン情報が存在する場合 */
    try {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      QuestionDAO questionDao = new QuestionDAO();

      // クリックされた質問ステータスのID(1、または2)を取得
      int statusId = Integer.parseInt(request.getParameter("status_id"));


      // 検索キーワードを取得
      String searchKeyword = (String) session.getAttribute("searchKeyword");



      // 「回答受付中」がクリックされた場合
      if (statusId == 1) {
        /*
         * 質問タイトルに検索キーワードを含み、かつ、質問ステータスが 「回答受付中」となっている質問を投稿日時が新しい順に10個取得する
         */
        List<Question> openQuestionList =
            questionDao.findQuestionsByTitleAndStatusId(searchKeyword, 1, 10, 0);

        // 質問の表示方法を示すID。1の場合、回答受付中と解決済みの質問を区別せず表示。2の場合、回答受付中の質問のみを表示。3の場合、解決済みの質問のみを表示。
        int displayMethod = 2;

        // 検索キーワードを含み、質問ステータスが「回答受付中」となっている質問の総数
        int openQuestionCount = (int) session.getAttribute("openQuestionCount");

        // ページネーションにおいて必要なページ数
        int maxPageNumber = (openQuestionCount + 9) / 10;

        for (Question question : openQuestionList) {
          System.out.println(question.getTitle());
        }



        request.setAttribute("questionList", openQuestionList);
        request.setAttribute("displayMethod", displayMethod);
        request.setAttribute("maxPageNumber", maxPageNumber);
        // ページネーションにおいて、遷移後のページ番号1を光らせるためにリクエストスコープに設定
        request.setAttribute("currentPageNumber", 1);

      }
      // 「解決済み」がクリックされた場合
      else if (statusId == 2) {
        /*
         * 質問タイトルに検索キーワードを含み、かつ、質問ステータスが 「解決済み」となっている質問を投稿日時が新しい順に10個取得する
         */
        List<Question> closeQuestionList =
            questionDao.findQuestionsByTitleAndStatusId(searchKeyword, 2, 10, 0);

        // 質問の表示方法を示すID。1の場合、回答受付中と解決済みの質問を区別せず表示。2の場合、回答受付中の質問のみを表示。3の場合、解決済みの質問のみを表示。
        int displayMethod = 3;

        // 検索キーワードを含み、質問ステータスが「解決済み」となっている質問の総数
        int closeQuestionCount = (int) session.getAttribute("closeQuestionCount");

        // ページネーションにおいて必要なページ数
        int maxPageNumber = (closeQuestionCount + 9) / 10;

        request.setAttribute("questionList", closeQuestionList);
        request.setAttribute("displayMethod", displayMethod);
        request.setAttribute("maxPageNumber", maxPageNumber);
        // ページネーションにおいて、遷移後のページ番号1を光らせるためにリクエストスコープに設定
        request.setAttribute("currentPageNumber", 1);
      }




      // searchResult.jspへフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/searchResult.jsp");
      dispatcher.forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
    }

  }



  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    doGet(request, response);
  }

}
