package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.QuestionDAO;
import model.Question;
import model.User;

/**
 * Servlet implementation class PaginationServlet
 */
@WebServlet("/SearchPaginationServlet")
public class SearchPaginationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchPaginationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
      HttpSession session = request.getSession();
      User loginUser = (User) session.getAttribute("loginUser");

      // ログイン情報が存在しない場合、LoginServletへリダイレクト
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }


      /* ログイン情報が存在する場合 */
      try {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // 検索キーワードを取得
        String searchKeyword = (String) session.getAttribute("searchKeyword");

        // 質問の表示方法を示すIDを取得
        int displayMethod = Integer.parseInt(request.getParameter("display_method"));

        // クリックされたページ番号を取得
        int pageNumber = Integer.parseInt(request.getParameter("page_number"));

        // ページネーションにおいて必要なページ数を取得
        int maxPageNumber = Integer.parseInt(request.getParameter("max_page_Number"));


        QuestionDAO questionDao = new QuestionDAO();

        // displayMethodが1の場合、回答受付中と解決済みの質問を区別せずまとめて取得
        if (displayMethod == 1) {
          // 指定されたページ番号に対応する「質問タイトルに検索キーワードを含む質問」を取得する
          List<Question> questionList =
              questionDao.findQuestionsByTitle(searchKeyword, 10, pageNumber * 10 - 10);

          request.setAttribute("questionList", questionList);
        }
        // displayMethodが2の場合、回答受付中の質問のみを取得
        else if (displayMethod == 2) {
          // 指定されたページ番号に対応する「質問タイトルに検索キーワードを含む回答受付中の質問」を取得する
          List<Question> openQuestionList = questionDao
              .findQuestionsByTitleAndStatusId(searchKeyword, 1, 10, pageNumber * 10 - 10);

          request.setAttribute("questionList", openQuestionList);
        }
        // displayMethodが3の場合、解決済みの質問のみを取得
        else if (displayMethod == 3) {
          // 指定されたページ番号に対応する「質問タイトルに検索キーワードを含む解決済みの質問」を取得する
          List<Question> closeQuestionList = questionDao
              .findQuestionsByTitleAndStatusId(searchKeyword, 2, 10, pageNumber * 10 - 10);

          request.setAttribute("questionList", closeQuestionList);
        }


        request.setAttribute("displayMethod", displayMethod);
        request.setAttribute("maxPageNumber", maxPageNumber);
        request.setAttribute("currentPageNumber", pageNumber);

        // searchResult.jspへフォワード
        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/searchResult.jsp");
        dispatcher.forward(request, response);
      } catch (Exception e) {
        e.printStackTrace();
      }

    }
	  

	  

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
