package controller;


import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDAO;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public LoginServlet() {
    super();


  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {


    HttpSession session = request.getSession();
    // ロゴがクリックされた際にセッションに保存されているエラーメッセージを削除する。
    session.removeAttribute("errMsg");
    // ロゴがクリックされた際にセッションに保存されている登録ユーザ情報を削除する
    session.removeAttribute("registUser");

    /*
     * ロゴがクリックされた際にセッションに保存されている質問情報と回答一覧情報、コメント一覧情報、回答数(質問詳細画面を表示するために必要な情報) を削除する
     */
    session.removeAttribute("questionInfo");
    session.removeAttribute("answerList");
    session.removeAttribute("commentList");
    session.removeAttribute("answerCount");

    /*
     * ロゴがクリックされた際にセッションに保存されている検索画面情報を削除する。
     */
    session.removeAttribute("searchKeyword");
    session.removeAttribute("questionCount");
    session.removeAttribute("openQuestionCount");
    session.removeAttribute("closeQuestionCount");


    User loginUser = (User) session.getAttribute("loginUser");


    // ログイン情報が存在する場合はQuestionListServletへリダイレクトする
    if (loginUser != null) {
      response.sendRedirect("QuestionListServlet");
      return;
    }

    /* ログイン情報が存在しない場合 */

    // ログアウトしたかどうかを表すパラメータを取得(ログアウトはtrue)
    boolean isLogout = Boolean.parseBoolean(request.getParameter("isLogout"));

    request.setAttribute("isLogout", isLogout);

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");
    
    String email = request.getParameter("email");
    String password=request.getParameter("password");
    
    //パスワード暗号化
    String encorderdPassword=PasswordEncorder.encordPassword(password);

    UserDAO userDao=new UserDAO();
    
    // 入力されたメールアドレスとパスワードを持つユーザがいるか検索
    User user = userDao.findByLoginInfo(email, encorderdPassword);

    // 入力されたメールアドレスとパスワードを持つユーザが見つからなかった場合
    if (user == null) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "メールアドレスまたはパスワードが異なります");
      request.setAttribute("email", email);

      // login.jspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
      dispatcher.forward(request, response);
      return;
    }

    /* 入力されたメールアドレスとパスワードを持つユーザが見つかった場合 */
    // ログインユーザ用のセッションを生成
    HttpSession session = request.getSession();
    

    // セッションにログインユーザの情報をセット
    session.setAttribute("loginUser", user);

    // QuestionListServletにリダイレクト
    response.sendRedirect("QuestionListServlet");
  }

}
