package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.AnswerDAO;
import dao.CommentDAO;
import model.Answer;
import model.QuestionAndUserAndStatus;
import model.User;

/**
 * Servlet implementation class CommentDeleteConfirmServlet
 */
@WebServlet("/AnswerDeleteConfirmServlet")
public class AnswerDeleteConfirmServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public AnswerDeleteConfirmServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // ログイン情報が存在しない場合、LoginServletへリダイレクト
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }


    /* ログイン情報が存在する場合 */
    try {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // 削除する回答のIDを取得
      String answerId = request.getParameter("answer_id");

      AnswerDAO answerDao = new AnswerDAO();

      // 削除対象の回答情報を取得
      Answer answerInfo = answerDao.findAnswerByAnswerId(Integer.parseInt(answerId));

      // 削除対象の回答文を取得
      String answer = answerInfo.getAnswer();

      request.setAttribute("answerId", answerId);
      request.setAttribute("answer", answer);

      // postDeleteConfirm.jspへフォワード
      RequestDispatcher dispatcher =
          request.getRequestDispatcher("/WEB-INF/jsp/postDeleteConfirm.jsp");
      dispatcher.forward(request, response);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // 削除する回答のIDを取得
    int answerId = Integer.parseInt(request.getParameter("answer_id"));

    HttpSession session = request.getSession();

    try {
      CommentDAO commentDao = new CommentDAO();
      AnswerDAO answerDao = new AnswerDAO();

      // commentテーブルより削除対象の回答IDを持つレコードを全て削除する。
      commentDao.deleteByAnswerId(answerId);

      // answerテーブルより削除対象の回答IDを持つレコードを削除する。
      answerDao.deleteByAnswerId(answerId);

      // クリックされた質問の情報を取得
      QuestionAndUserAndStatus questionInfo =
          (QuestionAndUserAndStatus) session.getAttribute("questionInfo");

      // クリックされた質問の質問IDを取得
      int questionId = questionInfo.getQuestionId();

      // QuestionDetailServletにリダイレクト
      response.sendRedirect("QuestionDetailServlet?question_id=" + questionId);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}