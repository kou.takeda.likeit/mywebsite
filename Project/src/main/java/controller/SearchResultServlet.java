package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.QuestionDAO;
import model.Question;
import model.User;

/**
 * Servlet implementation class SearchResultServlet
 */
@WebServlet("/SearchResultServlet")
public class SearchResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchResultServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  HttpSession session = request.getSession();
      User loginUser = (User) session.getAttribute("loginUser");

      // ログイン情報が存在しない場合、LoginServletへリダイレクト
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }


      /* ログイン情報が存在する場合 */
      try {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //検索キーワードを取得
        String searchKeyword=request.getParameter("search_keyword");
        
        QuestionDAO questionDao=new QuestionDAO();
        

        // 質問タイトルに検索キーワードを含む質問を投稿日時が新しい順に10個取得する
        List<Question> questionList = questionDao.findQuestionsByTitle(searchKeyword, 10, 0);

        // 検索キーワードを含む質問の総数
        int questionCount = questionDao.countQuestionsByTitle(searchKeyword);

        // 検索キーワードを含み、質問ステータスが「回答受付中」となっている質問の総数
        int openQuestionCount = questionDao.countQuestionsByTitleAndStatusId(searchKeyword, 1);

        // 質問ステータスが「解決済み」となっている質問の取得数
        int closeQuestionCount = questionDao.countQuestionsByTitleAndStatusId(searchKeyword, 2);

        // 質問の表示方法を示すID。1の場合、回答受付中と解決済みの質問を区別せず表示。2の場合、回答受付中の質問のみを表示。3の場合、解決済みの質問のみを表示。
        int displayMethod = 1;

        // ページネーションにおいて必要なページ数
        int maxPageNumber = (questionCount + 9) / 10;

        session.setAttribute("searchKeyword", searchKeyword);
        request.setAttribute("questionList", questionList);
        session.setAttribute("questionCount", questionCount);

        session.setAttribute("openQuestionCount", openQuestionCount);

        session.setAttribute("closeQuestionCount", closeQuestionCount);
        
        request.setAttribute("displayMethod", displayMethod);

        request.setAttribute("maxPageNumber", maxPageNumber);

        // ページネーションにおいて、遷移後のページ番号1を光らせるためにリクエストスコープに設定
        request.setAttribute("currentPageNumber", 1);

        // searchResult.jspへフォワード
        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/searchResult.jsp");
        dispatcher.forward(request, response);
      } catch (Exception e) {
        e.printStackTrace();
      }

    }



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

}
