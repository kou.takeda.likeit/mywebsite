package controller;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDAO;
import model.User;


/**
 * Servlet implementation class RegistConfirmServlet
 */
@WebServlet("/RegistConfirmServlet")
public class RegistConfirmServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public RegistConfirmServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {


  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    try {
      String name = request.getParameter("user_name");
      String email = request.getParameter("email");
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("password_confirm");

      User user = new User();
      user.setName(name);
      user.setEmail(email);
      user.setPassword(password);

      UserDAO userDao = new UserDAO();

      String errMsg = "";

      // 入力項目に1つでも未入力のものがある場合
      if (!isInputAll(name, email, password, passwordConfirm)) {
        errMsg += "未入力の項目があります<br>";
      }

      // 入力されているパスワードが確認用と異なる場合
      if (!(password.equals(passwordConfirm))) {
        errMsg += "パスワードとパスワード(確認)の内容が異なります<br>";
      }


      // ユーザ名が重複している場合
      if (userDao.isOverlapUserName(name)) {
        errMsg += "入力されたユーザ名は既に使用されています<br>";
      }

      // 入力されたメールアドレスの形式が不正な場合
      if (!(isValidEmail(email))) {
        errMsg += "メールアドレスの形式が正しくありません<br>";
      }
      // 入力されたメールアドレスの形式が正しい場合
      else {

        // メールアドレスが重複している場合
        if (userDao.isOverlapEmail(email)) {
          errMsg += "入力された情報が正しくありません";
        }
      }

      // セッションに登録ユーザの情報をセット
      session.setAttribute("registUser", user);

      // エラーメッセージがない場合、registConfirm.jspへフォワード
      if (errMsg.length() == 0) {

        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/registConfirm.jsp");
        dispatcher.forward(request, response);

      }
      // エラーメッセージがある場合、RegistServletへリダイレクト
      else {
        session.setAttribute("errMsg", errMsg);

        response.sendRedirect("RegistServlet");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * 新規登録画面に入力された値が一つでも空ならfalse、全て入力されていればtrueを返す。
   *
   */
  private static boolean isInputAll(String name, String email, String password,
      String passwordConfirm) {

    return !(name.equals("") || email.equals("") || password.equals("")
        || passwordConfirm.equals(""));

  }


  // 入力されたメールアドレスの形式が正しい場合trueを返す
  private static boolean isValidEmail(String email) {

    // 電子メールアドレスの正規表現パターン
    final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    // 正規表現パターンをコンパイルする
    final Pattern pattern = Pattern.compile(EMAIL_PATTERN);

    // 入力されたメールアドレスに対して、正規表現パターンを適用する
    Matcher matcher = pattern.matcher(email);

    // メールアドレスの形式が正規表現パターンに一致するかどうかをチェックし、その結果を返す。
    return matcher.matches();

  }

}
