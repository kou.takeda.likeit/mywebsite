package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.QuestionDAO;
import model.Question;
import model.User;

/**
 * Servlet implementation class QuestionList
 */
@WebServlet("/QuestionListServlet")
public class QuestionListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public QuestionListServlet() {
    super();

  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // ログイン情報が存在しない場合、LoginServletへリダイレクト
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }


    /* ログイン情報が存在する場合、質問を最大10件取得し、questionList.jspへフォワード */
    try {
      QuestionDAO questionDao = new QuestionDAO();

      // 質問を最大10件取得する
      List<Question> questionList = questionDao.findQuestions(10);

      // リクエストスコープに質問一覧情報をセット
      request.setAttribute("questionList", questionList);

      // questionList.jspへフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/questionList.jsp");
      dispatcher.forward(request, response);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // 指定された質問ステータスIDを取得
    String statusId = request.getParameter("status_id");

    // 指定された質問ステータスIDを持つ質問を最大10件取得し、questionList.jspへフォワード */
    try {
      QuestionDAO questionDao = new QuestionDAO();

      // 指定された質問ステータスIDを持つ質問を最大10件取得する
      List<Question> questionList =
          questionDao.findQuestionsByStatusId(Integer.parseInt(statusId), 10);

      // リクエストスコープに質問一覧情報をセット
      request.setAttribute("questionList", questionList);

      if (statusId.equals("1")) {
        request.setAttribute("highLightStatus", "open");
      } else if (statusId.equals("2")) {
        request.setAttribute("highLightStatus", "close");
      }



      // questionList.jspへフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/questionList.jsp");
      dispatcher.forward(request, response);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}
