package model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * userテーブルのデータを格納するためのBeans
 */

public class User implements Serializable {
  private int id;
  private String name;
  private String email;
  private String password;
  private Timestamp createDate;


  public User() {

  }


  public User(int id, String name, String email, String encorderdPassword, Timestamp createDate) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = encorderdPassword;
    this.createDate = createDate;
  }


  public int getId() {
    return id;
  }


  public void setId(int id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }


  public void setName(String name) {
    this.name = name;
  }


  public String getEmail() {
    return email;
  }


  public void setEmail(String email) {
    this.email = email;
  }


  public String getPassword() {
    return password;
  }


  public void setPassword(String password) {
    this.password = password;
  }


  public Timestamp getCreateDate() {
    return createDate;
  }


  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }



}
