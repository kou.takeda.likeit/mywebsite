package model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * answerテーブルのデータを格納するためのBeans
 */

public class Answer implements Serializable {
  private int id;
  private int userId;
  private String answer;
  private Timestamp createDate;
  private int questionId;


  public Answer() {

  }

  public Answer(int id, int userId, String answer, Timestamp createDate, int questionId) {
    this.id = id;
    this.userId = userId;
    this.answer = answer;
    this.createDate = createDate;
    this.questionId = questionId;
  }


  public int getId() {
    return id;
  }


  public void setId(int id) {
    this.id = id;
  }


  public int getUserId() {
    return userId;
  }


  public void setUserId(int userId) {
    this.userId = userId;
  }


  public String getAnswer() {
    return answer;
  }


  public void setAnswer(String answer) {
    this.answer = answer;
  }


  public Timestamp getCreateDate() {
    return createDate;
  }


  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }


  public int getQuestionId() {
    return questionId;
  }


  public void setQuestionId(int questionId) {
    this.questionId = questionId;
  }

}
