package model;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * commentテーブルのデータを格納するためのBeans
 */

public class Comment implements Serializable {
  private int id;
  private int userId;
  private String comment;
  private Timestamp createDate;
  private int questionId;
  private int answerId;



  public Comment() {

  }

  public Comment(int id, int userId, String comment, Timestamp createDate, int questionId,
      int answerId) {
    this.id = id;
    this.userId = userId;
    this.comment = comment;
    this.createDate = createDate;
    this.questionId = questionId;
    this.answerId = answerId;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public int getQuestionId() {
    return questionId;
  }

  public void setQuestionId(int questionId) {
    this.questionId = questionId;
  }

  public int getAnswerId() {
    return answerId;
  }

  public void setAnswerId(int answerId) {
    this.answerId = answerId;
  }

}
