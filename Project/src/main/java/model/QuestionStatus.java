package model;

import java.io.Serializable;

/**
 * question_statusテーブルのデータを格納するためのBeans
 */

public class QuestionStatus implements Serializable {
  private int id;
  private String status;


  public QuestionStatus() {

  }


  public QuestionStatus(int id, String status) {
    this.id = id;
    this.status = status;
  }

  public int getId() {
    return id;
  }


  public void setId(int id) {
    this.id = id;
  }


  public String getStatus() {
    return status;
  }


  public void setStatus(String status) {
    this.status = status;
  }

}