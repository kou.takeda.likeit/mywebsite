package model;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * questionテーブルのデータを格納するためのBeans
 */

public class Question implements Serializable {
  private int id;
  private int userId;
  private String title;
  private String question;
  private Timestamp createDate;
  private int statusId;


  public Question() {

  }



  public Question(int id, int userId, String title, String question, Timestamp createDate,
      int statusId) {

    this.id = id;
    this.userId = userId;
    this.title = title;
    this.question = question;
    this.createDate = createDate;
    this.statusId = statusId;
  }



  public int getId() {
    return id;
  }


  public void setId(int id) {
    this.id = id;
  }


  public int getUserId() {
    return userId;
  }


  public void setUserId(int userId) {
    this.userId = userId;
  }


  public String getTitle() {
    return title;
  }


  public void setTitle(String title) {
    this.title = title;
  }


  public String getQuestion() {
    return question;
  }


  public void setQuestion(String question) {
    this.question = question;
  }


  public Timestamp getCreateDate() {
    return createDate;
  }


  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }


  public int getStatusId() {
    return statusId;
  }


  public void setStatusId(int statusId) {
    this.statusId = statusId;
  }


}
