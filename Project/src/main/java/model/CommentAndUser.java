package model;

import java.io.Serializable;
import java.sql.Timestamp;

// commentテーブルとuserテーブルを結合したテーブルのデータを格納するためのBeans
public class CommentAndUser implements Serializable {
  private int commentId;
  private int commentUserId;
  private String comment;
  private Timestamp commentCreateDate;
  private int questionId;
  private int answerId;
  private String commentUserName;
  private String commentUserEmail;
  private String commentUserPassword;
  private Timestamp commentUserCreateDate;

  public CommentAndUser() {

  }

  public CommentAndUser(int commentId, int commentUserId, String comment, Timestamp commentCreateDate,
      int questionId, int answerId, String commentUserName, String commentUserEmail,
      String commentUserPassword, Timestamp commentUserCreateDate) {
    super();
    this.commentId = commentId;
    this.commentUserId = commentUserId;
    this.comment = comment;
    this.commentCreateDate = commentCreateDate;
    this.questionId = questionId;
    this.answerId = answerId;
    this.commentUserName = commentUserName;
    this.commentUserEmail = commentUserEmail;
    this.commentUserPassword = commentUserPassword;
    this.commentUserCreateDate = commentUserCreateDate;
  }

  public int getCommentId() {
    return commentId;
  }

  public void setCommentId(int commentId) {
    this.commentId = commentId;
  }

  public int getCommentUserId() {
    return commentUserId;
  }

  public void setCommentUserId(int commentUserId) {
    this.commentUserId = commentUserId;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Timestamp getCommentCreateDate() {
    return commentCreateDate;
  }

  public void setCommentCreateDate(Timestamp commentCreateDate) {
    this.commentCreateDate = commentCreateDate;
  }

  public int getQuestionId() {
    return questionId;
  }

  public void setQuestionId(int questionId) {
    this.questionId = questionId;
  }

  public int getAnswerId() {
    return answerId;
  }

  public void setAnswerId(int answerId) {
    this.answerId = answerId;
  }

  public String getCommentUserName() {
    return commentUserName;
  }

  public void setCommentUserName(String commentUserName) {
    this.commentUserName = commentUserName;
  }

  public String getCommentUserEmail() {
    return commentUserEmail;
  }

  public void setCommentUserEmail(String commentUserEmail) {
    this.commentUserEmail = commentUserEmail;
  }

  public String getCommentUserPassword() {
    return commentUserPassword;
  }

  public void setCommentUserPassword(String commentUserPassword) {
    this.commentUserPassword = commentUserPassword;
  }

  public Timestamp getCommentUserCreateDate() {
    return commentUserCreateDate;
  }

  public void setCommentUserCreateDate(Timestamp commentUserCreateDate) {
    this.commentUserCreateDate = commentUserCreateDate;
  }



}
