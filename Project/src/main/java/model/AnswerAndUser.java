package model;

import java.io.Serializable;
import java.sql.Timestamp;

// answerテーブルとuserテーブルを結合したテーブルのデータを格納するためのBeans
public class AnswerAndUser implements Serializable {
  private int answerId;
  private int answerUserId;
  private String answer;
  private Timestamp answerCreateDate;
  private int questionId;
  private String answerUserName;
  private String answerUserEmail;
  private String answerUserPassword;
  private Timestamp answerUserCreateDate;

  public AnswerAndUser() {

  }

  public AnswerAndUser(int answerId, int answerUserId, String answer, Timestamp answerCreateDate,
      int questionId, String answerUserName, String answerUserEmail, String answerUserPassword,
      Timestamp answerUserCreateDate) {
    super();
    this.answerId = answerId;
    this.answerUserId = answerUserId;
    this.answer = answer;
    this.answerCreateDate = answerCreateDate;
    this.questionId = questionId;
    this.answerUserName = answerUserName;
    this.answerUserEmail = answerUserEmail;
    this.answerUserPassword = answerUserPassword;
    this.answerUserCreateDate = answerUserCreateDate;
  }

  public int getAnswerId() {
    return answerId;
  }

  public void setAnswerId(int answerId) {
    this.answerId = answerId;
  }

  public int getAnswerUserId() {
    return answerUserId;
  }

  public void setAnswerUserId(int answerUserId) {
    this.answerUserId = answerUserId;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public Timestamp getAnswerCreateDate() {
    return answerCreateDate;
  }

  public void setAnswerCreateDate(Timestamp answerCreateDate) {
    this.answerCreateDate = answerCreateDate;
  }

  public int getQuestionId() {
    return questionId;
  }

  public void setQuestionId(int questionId) {
    this.questionId = questionId;
  }

  public String getAnswerUserName() {
    return answerUserName;
  }

  public void setAnswerUserName(String answerUserName) {
    this.answerUserName = answerUserName;
  }

  public String getAnswerUserEmail() {
    return answerUserEmail;
  }

  public void setAnswerUserEmail(String answerUserEmail) {
    this.answerUserEmail = answerUserEmail;
  }

  public String getAnswerUserPassword() {
    return answerUserPassword;
  }

  public void setAnswerUserPassword(String answerUserPassword) {
    this.answerUserPassword = answerUserPassword;
  }

  public Timestamp getAnswerUserCreateDate() {
    return answerUserCreateDate;
  }

  public void setAnswerUserCreateDate(Timestamp answerUserCreateDate) {
    this.answerUserCreateDate = answerUserCreateDate;
  }



}
