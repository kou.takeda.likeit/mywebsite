package model;

import java.io.Serializable;
import java.sql.Timestamp;

// questionテーブルとuserテーブルとquestion_statusテーブルを結合したテーブルのデータを格納するためのBeans
public class QuestionAndUserAndStatus implements Serializable {
  private int questionId;
  private int questionUserId;
  private String title;
  private String question;
  private Timestamp questionCreateDate;
  private int statusId;
  private String status;
  private String questionUserName;
  private String questionUserEmail;
  private String questionUserPassword;
  private Timestamp questionUserCreateDate;

  public QuestionAndUserAndStatus() {

  }

  public QuestionAndUserAndStatus(int questionId, int questionUserId, String title, String question,
      Timestamp questionCreateDate, int statusId, String status, String questionUserName,
      String questionUserEmail, String questionUserPassword, Timestamp questionUserCreateDate) {

    this.questionId = questionId;
    this.questionUserId = questionUserId;
    this.title = title;
    this.question = question;
    this.questionCreateDate = questionCreateDate;
    this.statusId = statusId;
    this.status = status;
    this.questionUserName = questionUserName;
    this.questionUserEmail = questionUserEmail;
    this.questionUserPassword = questionUserPassword;
    this.questionUserCreateDate = questionUserCreateDate;
  }

  public int getQuestionId() {
    return questionId;
  }

  public void setQuestionId(int questionId) {
    this.questionId = questionId;
  }

  public int getQuestionUserId() {
    return questionUserId;
  }

  public void setQuestionUserId(int questionUserId) {
    this.questionUserId = questionUserId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public Timestamp getQuestionCreateDate() {
    return questionCreateDate;
  }

  public void setQuestionCreateDate(Timestamp questionCreateDate) {
    this.questionCreateDate = questionCreateDate;
  }

  public int getStatusId() {
    return statusId;
  }

  public void setStatusId(int statusId) {
    this.statusId = statusId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getQuestionUserName() {
    return questionUserName;
  }

  public void setQuestionUserName(String questionUserName) {
    this.questionUserName = questionUserName;
  }

  public String getQuestionUserEmail() {
    return questionUserEmail;
  }

  public void setQuestionUserEmail(String questionUserEmail) {
    this.questionUserEmail = questionUserEmail;
  }

  public String getQuestionUserPassword() {
    return questionUserPassword;
  }

  public void setQuestionUserPassword(String questionUserPassword) {
    this.questionUserPassword = questionUserPassword;
  }

  public Timestamp getQuestionUserCreateDate() {
    return questionUserCreateDate;
  }

  public void setQuestionUserCreateDate(Timestamp questionUserCreateDate) {
    this.questionUserCreateDate = questionUserCreateDate;
  }



}
