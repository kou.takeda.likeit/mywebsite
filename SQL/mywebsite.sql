﻿CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;

USE mywebsite;

CREATE TABLE user
(id INT PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(255) NOT NULL,
email VARCHAR(255) UNIQUE NOT NULL,
password VARCHAR(255) NOT NULL,
create_date DATETIME NOT NULL
);

CREATE TABLE question_status
(id INT PRIMARY KEY,
status VARCHAR(255) UNIQUE NOT NULL
);

INSERT INTO question_status VALUES(1,'回答受付中');
INSERT INTO question_status VALUES(2,'解決済み');


CREATE TABLE question
(id INT PRIMARY KEY AUTO_INCREMENT,
user_id INT NOT NULL,
title VARCHAR(255) NOT NULL,
question TEXT NOT NULL,
create_date DATETIME NOT NULL,
status_id INT NOT NULL DEFAULT 1,
FOREIGN KEY(user_id) REFERENCES user(id),
FOREIGN KEY(status_id) REFERENCES question_status(id)
);

CREATE TABLE answer
(id INT PRIMARY KEY AUTO_INCREMENT,
user_id INT NOT NULL,
answer TEXT NOT NULL,
create_date DATETIME NOT NULL,
question_id INT NOT NULL,
FOREIGN KEY(user_id) REFERENCES user(id),
FOREIGN KEY(question_id) REFERENCES question(id)
);

CREATE TABLE comment
(id INT PRIMARY KEY AUTO_INCREMENT,
user_id INT NOT NULL,
comment TEXT NOT NULL,
create_date DATETIME NOT NULL,
question_id INT NOT NULL,
answer_id INT NOT NULL,
FOREIGN KEY(user_id) REFERENCES user(id),
FOREIGN KEY(question_id) REFERENCES question(id),
FOREIGN KEY(answer_id) REFERENCES answer(id)
);
